const Sentry = require('@sentry/node');
// Sentry.init({ });

const express = require('express');
const winston = require('winston');
const { SentryCompatTransport } = require('./index.js');

const logger = new winston.Logger();
logger.add(winston.transports.Console, {
  level: 'debug',
  colorize: true,
});

// Required: capture log messages (config/log.js)
logger.add(SentryCompatTransport, {
  install: true,
});

const app = express();

// Required: capture request information (config/http.js)
app.use(Sentry.Handlers.requestHandler());

// Optional: Example user information capture middleware (config/http.js)
app.use((req, res, next) => {
  Sentry.configureScope((scope) => {
    scope.setUser({
      id: req.query.user || 2137,
      network: 42,
      email: 'testing@m4b.pl',
    });
  });
  next();
});

function testThrow() {
  throw new Error('Something?');
}

app.get('/', async (req, res) => {
  logger.info('Hello!');
  logger.error('First error');
  try {
    testThrow();
  } catch(e) {
    logger.error('Some error occured', {test: 'xD'}, e);
  }

  res.send('Hello!');
});

app.get('/justlog', (req, res) => {
  logger.error('Just error log');
  res.send('OK');
});

app.get('/throw', (req, res) => {
  throw new Error('dupa');
});

// Test request breadcrumb scoping
app.get('/longreq', (req, res) => {
  logger.info('Just before a long request');
  setTimeout(() => {
    logger.info('Just after a long request');
    res.send('OK');
  }, 3000);
});

app.get('/longerr', (req, res) => {
  logger.info('Just before a long error');
  setTimeout(() => {
    try {
      testThrow();
    } catch (e) {
      logger.error('Some error occured', e);
    }
    logger.info('Just after a long error');
    res.send('OK');
  }, 2000);
});

// Required: capture errors
app.use(Sentry.Handlers.errorHandler());

app.listen(3000);
