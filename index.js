let TransportStream;

/* eslint-disable global-require, import/no-unresolved */
try {
  TransportStream = require('winston-transport');
} catch (e) {
  TransportStream = require('winston').Transport;
}
/* eslint-enable */

const Sentry = require('@sentry/node');


/**
 * Winston@3 transport
 */
class SentryTransport extends TransportStream {
  constructor(options = {}) {
    super(options);

    // Forward plain error messages as exceptions
    this.handleError = options.handleError;

    // Special filtering callback, return false for messages to be ignored by
    // winston-sentry4
    this.filter = options.filter;

    this.splatExtractLevels = options.splatExtractLevels || ['error'];

    if (options.install) {
      options.sentryOptions = options.sentryOptions || {};
      Sentry.init({ dsn: options.dsn || process.env.SENTRY_DSN, ...options.sentryOptions });
    }
  }

  log(info, callback) {
    if (this.filter && !this.filter(info)) {
      callback(null, true);
      return;
    }

    const { message } = info;
    const level = info[Symbol.for('level')];
    const splat = info[Symbol.for('splat')] || [];

    // Make multiple splat values somehow readable
    const data = splat.length > 1 ? splat.map((v, k) => {
      if (v instanceof Error) {
        return [k, v.stack];
      }

      return [k, v];
    }).reduce((r, k) => { return { [k[0]]: k[1], ...r }; }, {}) : splat[0];

    Sentry.addBreadcrumb({
      data,
      category: 'log',
      message,
      level,
    });

    let exceptionFound = false;

    if (this.splatExtractLevels.includes(level)) {
      // eslint-disable-next-line no-restricted-syntax
      for (const item of splat) {
        if (item instanceof Error) {
          Sentry.captureException(item);
          exceptionFound = true;
        }
      }
    }

    if (!exceptionFound && level === 'error' && this.handleError) {
      Sentry.captureException(new Error(message));
    }

    callback(null, true);
  }
}

/**
 * Winston@2 compatiblity
 */
class SentryCompatTransport extends SentryTransport {
  log(level, message, meta, callback) {
    super.log({
      message,
      [Symbol.for('level')]: level,
      [Symbol.for('splat')]: (meta instanceof Error || Object.keys(meta).length) ? [meta] : [],
    }, callback);
  }
}

module.exports = { SentryTransport, SentryCompatTransport };
