winston-sentry4
===============

Sentry-based winston (2/3) logging transport with support for breadcrumbs

Installation
------------

    npm install --save @sentry/node@4.x
    npm install --save winston@3.x # or... winston@2.x
    npm install --save https://bitbucket.org/m4bpl/winston-sentry4/get/v1.0.0.tar.gz

Usage
-----

Quick and simple usage examples in `express` environment are available in
`example.js` (for `winston@3`) and `example.winston2.js`.

`SentryTransport`/`SentryCompatTransport` accept following configuration options:

 * `install` - automatically initialize Sentry client (default: `false`)
 * `dsn` - use this dsn when automatically initializing DSN (default: `process.env.SENTRY_DSN`)
